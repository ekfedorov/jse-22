package ru.ekfedorov.tm.command.project;

import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String name = TerminalUtil.nextLine();
        if (isEmpty(name)) name = "project";
        System.out.println("ENTER DESCRIPTION:");
        String description = TerminalUtil.nextLine();
        if (isEmpty(description)) description = "without description";
        final Project project = serviceLocator.getProjectService().add(userId, name, description);
        if (project == null) throw new NullProjectException();
        System.out.println();
    }

    @Override
    public String name() {
        return "project-create";
    }

}
