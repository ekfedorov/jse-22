package ru.ekfedorov.tm.command.project;

import ru.ekfedorov.tm.command.AbstractProjectCommand;

public class ProjectClearAllCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear all project.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR]");
        serviceLocator.getProjectService().clear(userId);
        System.out.println("--- successfully cleared ---\n");
    }

    @Override
    public String name() {
        return "project-clear";
    }

}
