package ru.ekfedorov.tm.command.system;

import ru.ekfedorov.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Evgeniy Fedorov");
        System.out.println("ekfedorov@tsconsulting.com");
    }

    @Override
    public String name() {
        return "about";
    }

}
