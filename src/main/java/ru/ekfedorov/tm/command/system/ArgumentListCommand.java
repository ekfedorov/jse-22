package ru.ekfedorov.tm.command.system;

import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        final Collection<String> arguments = serviceLocator.getCommandService().getListArgumentName();
        System.out.println("[ARGUMENTS]");
        for (final String argument : arguments) {
            if (argument != null) System.out.println(argument);
        }
    }

    @Override
    public String name() {
        return "arguments";
    }

}
