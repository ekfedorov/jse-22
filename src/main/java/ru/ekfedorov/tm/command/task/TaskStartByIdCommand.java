package ru.ekfedorov.tm.command.task;

import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start task by id.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Optional<Task> task = serviceLocator.getTaskService().startById(userId, id);
        if (!task.isPresent()) throw new NullTaskException();
    }

    @Override
    public String name() {
        return "start-task-by-id";
    }

}
