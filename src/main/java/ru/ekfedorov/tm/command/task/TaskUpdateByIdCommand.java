package ru.ekfedorov.tm.command.task;

import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        final Optional<Task> task = taskService.findOneById(userId, id);
        if (!task.isPresent()) throw new NullTaskException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Optional<Task> taskUpdated = taskService.updateById(userId, id, name, description);
        if (!taskUpdated.isPresent()) throw new NullTaskException();
    }

    @Override
    public String name() {
        return "task-update-by-id";
    }

}
