package ru.ekfedorov.tm.command.user;

import ru.ekfedorov.tm.command.AbstractUserCommand;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final String userId = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeOneById(userId);
    }

    @Override
    public String name() {
        return "remove-user-by-id";
    }

    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
