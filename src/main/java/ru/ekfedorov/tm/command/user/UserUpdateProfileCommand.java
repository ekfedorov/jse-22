package ru.ekfedorov.tm.command.user;

import ru.ekfedorov.tm.command.AbstractUserCommand;
import ru.ekfedorov.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update profile.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROFILE]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().userUpdate(userId, firstName, lastName, middleName);
    }

    @Override
    public String name() {
        return "update-profile";
    }

}
