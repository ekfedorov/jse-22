package ru.ekfedorov.tm.command.user;

import ru.ekfedorov.tm.command.AbstractUserCommand;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Do logout.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String name() {
        return "logout";
    }

}
