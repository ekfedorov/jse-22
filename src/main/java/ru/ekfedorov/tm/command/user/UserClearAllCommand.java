package ru.ekfedorov.tm.command.user;

import ru.ekfedorov.tm.command.AbstractUserCommand;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.model.User;

import java.util.Optional;

public class UserClearAllCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear all users.";
    }

    @Override
    public void execute() throws Exception {
        final Optional<User> user = serviceLocator.getAuthService().getUser();
        if (!user.isPresent()) throw new UserIdIsEmptyException();
        System.out.println("[CLEAR ALL USERS]");
        serviceLocator.getUserService().clear();
    }

    @Override
    public String name() {
        return "user-clear-all";
    }

    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
