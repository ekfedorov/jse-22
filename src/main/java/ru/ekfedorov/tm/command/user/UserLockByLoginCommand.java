package ru.ekfedorov.tm.command.user;

import ru.ekfedorov.tm.command.AbstractUserCommand;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.util.TerminalUtil;

public class UserLockByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
