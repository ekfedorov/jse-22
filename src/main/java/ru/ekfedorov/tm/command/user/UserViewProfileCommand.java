package ru.ekfedorov.tm.command.user;

import ru.ekfedorov.tm.command.AbstractUserCommand;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.model.User;

import java.util.Optional;

public class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "View my profile.";
    }

    @Override
    public void execute() throws Exception {
        final Optional<User> userOptional = serviceLocator.getAuthService().getUser();
        if (!userOptional.isPresent()) throw new UserIdIsEmptyException();
        final User user = userOptional.get();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("USER ID: " + user.getId());
    }

    @Override
    public String name() {
        return "view-profile";
    }

}
