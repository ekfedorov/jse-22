package ru.ekfedorov.tm.command.user;

import ru.ekfedorov.tm.command.AbstractUserCommand;

public class UserIsAuthCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Auth consist.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("User auth: " + serviceLocator.getAuthService().isAuth());
    }

    @Override
    public String name() {
        return "is-auth";
    }

}
