package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.ICommandRepository;
import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.*;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandArgs() {
        return commands.values().stream()
                .map(AbstractCommand::arg)
                .filter(arg -> !isEmpty(arg))
                .collect(Collectors.toList());
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return commands.get(arg);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commands.get(name);
    }

    @Override
    public Collection<String> getCommandName() {
        return commands.values().stream()
                .map(AbstractCommand::name)
                .filter(name -> !isEmpty(name))
                .collect(Collectors.toList());
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}
