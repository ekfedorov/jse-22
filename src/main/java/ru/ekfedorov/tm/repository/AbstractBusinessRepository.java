package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.IBusinessRepository;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.AbstractBusinessEntity;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public void clear(final String userId) {
        list.stream().filter(predicateByUserId(userId))
                .collect(Collectors.toList())
                .forEach(list::remove);

    }

    @Override
    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<E> findOneById(final String userId, final String id) {
        return list.stream()
                .filter(predicateByUserIdAndEntityId(userId, id))
                .findFirst();
    }

    @Override
    public Optional<E> findOneByIndex(final String userId, final Integer index) {
        return Optional.ofNullable(
                list.stream()
                        .filter(predicateByUserId(userId))
                        .collect(Collectors.toList())
                        .get(index)
        );
    }

    @Override
    public Optional<E> findOneByName(final String userId, final String name) {
        return list.stream()
                .filter(predicateByNameAndUserId(userId, name))
                .findFirst();
    }

    public Predicate<E> predicateByNameAndUserId(final String userId, final String name) {
        return e -> userId.equals(e.getUserId()) && name.equals(e.getName());
    }

    public Predicate<E> predicateByUserId(final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    public Predicate<E> predicateByUserIdAndEntityId(final String userId, final String entityId) {
        return e -> userId.equals(e.getUserId()) && entityId.equals(e.getId());
    }

    @Override
    public boolean remove(final String userId, final E entity) {
        return findOneById(userId, entity.getId())
                .map(list::remove)
                .orElse(false);
    }

    @Override
    public boolean removeOneById(final String userId, final String id) {
        return findOneById(userId, id)
                .map(list::remove)
                .orElse(false);
    }

    @Override
    public boolean removeOneByIndex(
            final String userId, final Integer index
    ) {
        return findOneByIndex(userId, index)
                .map(list::remove)
                .orElse(false);
    }

    @Override
    public boolean removeOneByName(
            final String userId, final String name
    ) {
        return findOneByName(userId, name)
                .map(list::remove)
                .orElse(false);
    }

}
