package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public Optional<Task> bindTaskByProjectId(
            final String userId, final String projectId, final String taskId
    ) {
        final Optional<Task> task = findOneById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(
            final String userId, final String projectId
    ) {
        if (isEmpty(projectId)) return null;
        final List<Task> list1 = new ArrayList<>();
        for (Task task : list) {
            if (projectId.equals(task.getProjectId()) &&
                    userId.equals(task.getUserId())) list1.add(task);
        }
        return list1;
    }

    @Override
    public void removeAllByProjectId(
            final String userId, final String projectId
    ) {
        if (isEmpty(projectId)) return;
        list.removeIf(
                task -> projectId.equals(task.getProjectId()) &&
                        userId.equals(task.getUserId())
        );
    }

    @Override
    public Optional<Task> unbindTaskFromProjectId(
            final String userId, final String taskId
    ) {
        final Optional<Task> task = findOneById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(null);
        return task;
    }

}
