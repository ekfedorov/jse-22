package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.model.AbstractEntity;
import ru.ekfedorov.tm.model.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public E add(final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        return list.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<E> findOneById(final String id) {
        return list.stream()
                .filter(entity -> id.equals(entity.getId()))
                .findFirst();
    }

    @Override
    public E remove(final E entity) {
        list.remove(entity);
        return entity;
    }

    @Override
    public Optional<E> removeOneById(final String id) {
        return findOneById(id).map(this::remove);
    }

}
