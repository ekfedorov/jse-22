package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IBusinessRepository;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IBusinessRepository<Task> {

    Optional<Task> bindTaskByProjectId(final String userId, String projectId, String taskId);

    List<Task> findAllByProjectId(final String userId, String projectId);

    void removeAllByProjectId(final String userId, String projectId);

    Optional<Task> unbindTaskFromProjectId(final String userId, String taskId);

}