package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    Optional<User> findByLogin(String login);

    Boolean isLoginExist(String login);

    void removeByLogin(String login);

}
