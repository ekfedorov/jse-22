package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.api.IBusinessService;
import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Project;

import java.util.Optional;

public interface IProjectService extends IBusinessService<Project> {

    Project add(final String userId, String name, String description) throws Exception;

}
