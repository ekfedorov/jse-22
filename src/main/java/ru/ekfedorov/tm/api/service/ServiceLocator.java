package ru.ekfedorov.tm.api.service;

public interface ServiceLocator {

    IAuthService getAuthService();

    ICommandService getCommandService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IUserService getUserService();

}
