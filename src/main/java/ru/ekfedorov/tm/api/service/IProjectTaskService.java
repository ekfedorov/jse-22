package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {

    Optional<Task> bindTaskByProject(final String userId, String projectId, String taskId) throws Exception;

    List<Task> findAllByProjectId(final String userId, String projectId) throws Exception;

    boolean removeProjectById(final String userId, String projectId) throws Exception;

    Optional<Task> unbindTaskFromProject(final String userId, String taskId) throws Exception;

}
