package ru.ekfedorov.tm.model;

import ru.ekfedorov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public abstract class AbstractBusinessEntity extends AbstractEntity {

    protected Date created = new Date();

    protected Date dateFinish;

    protected Date dateStart;

    protected String description = "";

    protected String name = "";

    protected Status status = Status.NOT_STARTED;

    private String userId;

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return id + ": " + name + " | " + status.getDisplayName();
    }

}

