package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.DescriptionIsEmptyException;
import ru.ekfedorov.tm.exception.empty.NameIsEmptyException;
import ru.ekfedorov.tm.model.Task;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(
            final String userId, final String name, final String description
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
        return task;
    }

}
