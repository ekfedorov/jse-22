package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.service.ILoggerService;
import ru.ekfedorov.tm.bootstrap.Bootstrap;

import java.io.IOException;
import java.util.logging.*;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class LoggerService implements ILoggerService {

    public static final String COMMANDS = "COMMANDS";
    public static final String COMMANDS_FILE = "./commands.txt";

    public static final String ERRORS = "ERRORS";
    public static final String ERRORS_FILE = "./errors.txt";

    public static final String MESSAGES = "MESSAGES";
    public static final String MESSAGES_FILE = "./messages.txt";

    public final Logger commands = Logger.getLogger(COMMANDS);
    public final Logger errors = Logger.getLogger(ERRORS);
    public final Logger messages = Logger.getLogger(MESSAGES);
    public final Logger root = Logger.getLogger("");
    private final LogManager manager = LogManager.getLogManager();

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(messages, MESSAGES_FILE, true);
        registry(errors, ERRORS_FILE, true);
    }

    @Override
    public void command(final String message) {
        if (isEmpty(message)) return;
        commands.info(message);
    }

    @Override
    public void debug(final String message) {
        if (isEmpty(message)) return;
        messages.fine(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    @Override
    public void info(final String message) {
        if (isEmpty(message)) return;
        messages.info(message);
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream("/logger.properties"));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(
            final Logger logger, final String fileName, final Boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

}
