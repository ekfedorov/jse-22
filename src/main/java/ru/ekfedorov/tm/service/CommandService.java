package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.repository.ICommandRepository;
import ru.ekfedorov.tm.api.service.ICommandService;
import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.Collection;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        if (isEmpty(arg)) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandName();
    }

}

