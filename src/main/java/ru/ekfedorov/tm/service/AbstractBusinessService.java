package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.IBusinessRepository;
import ru.ekfedorov.tm.api.IBusinessService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.NameIsEmptyException;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.AbstractBusinessEntity;


import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;
import static ru.ekfedorov.tm.util.ValidateUtil.notNullOrLessZero;

public class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    private final IBusinessRepository<E> businessRepository;

    public AbstractBusinessService(final IBusinessRepository<E> businessRepository) {
        super(businessRepository);
        this.businessRepository = businessRepository;
    }

    @Override
    public void clear(final String userId) {
        businessRepository.clear(userId);
    }

    @Override
    public List<E> findAll(final String userId) {
        return businessRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) throws Exception {
        if (comparator == null) throw new NullComparatorException();
        return businessRepository.findAll(userId, comparator);
    }

    @Override
    public Optional<E> findOneById(final String userId, final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return businessRepository.findOneById(userId, id);
    }

    @Override
    public boolean remove(final String userId, final E entity) throws Exception {
        if (entity == null) throw new NullObjectException();
        return businessRepository.remove(userId, entity);
    }

    @Override
    public boolean removeOneById(final String userId, final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return businessRepository.removeOneById(userId, id);
    }

    @Override
    public Optional<E> findOneByIndex(final String userId, final Integer index) throws Exception {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        return businessRepository.findOneByIndex(userId, index);
    }

    @Override
    public Optional<E> findOneByName(final String userId, final String name) throws Exception {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        return businessRepository.findOneByName(userId, name);
    }

    @Override
    public boolean removeOneByIndex(final String userId, final Integer index) throws Exception {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        return businessRepository.removeOneByIndex(userId, index);
    }

    @Override
    public boolean removeOneByName(final String userId, final String name) throws Exception {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        return businessRepository.removeOneByName(userId, name);
    }

    @Override
    public Optional<E> updateById(
            final String userId, final String id, final String name, final String description
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Optional<E> enity = findOneById(userId, id);
        if (!enity.isPresent()) throw new NullObjectException();
        enity.get().setName(name);
        enity.get().setDescription(description);
        return enity;
    }

    @Override
    public Optional<E> updateByIndex(
            final String userId, final Integer index, final String name, final String description
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Optional<E> enity = findOneByIndex(userId, index);
        if (!enity.isPresent()) throw new NullObjectException();
        enity.get().setName(name);
        enity.get().setDescription(description);
        return enity;
    }

    @Override
    public Optional<E> changeStatusById(
            final String userId, final String id, final Status status
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Optional<E> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        return entity;
    }

    @Override
    public Optional<E> changeStatusByIndex(
            final String userId, final Integer index, final Status status
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        return entity;
    }

    @Override
    public Optional<E> changeStatusByName(
            final String userId, final String name, final Status status
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Optional<E> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        return entity;
    }

    @Override
    public Optional<E> finishById(final String userId, final String id) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Optional<E> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    public Optional<E> finishByIndex(final String userId, final Integer index) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    public Optional<E> finishByName(final String userId, final String name) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Optional<E> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    public Optional<E> startById(final String userId, final String id) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Optional<E> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public Optional<E> startByIndex(final String userId, final Integer index) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public Optional<E> startByName(final String userId, final String name) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) return Optional.empty();
        final Optional<E> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        return entity;
    }

}
