package ru.ekfedorov.tm.bootstrap;

import ru.ekfedorov.tm.api.repository.ICommandRepository;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.command.project.*;
import ru.ekfedorov.tm.command.system.*;
import ru.ekfedorov.tm.command.task.*;

import ru.ekfedorov.tm.command.user.*;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.incorrect.IncorrectCommandException;
import ru.ekfedorov.tm.repository.CommandRepository;
import ru.ekfedorov.tm.repository.ProjectRepository;
import ru.ekfedorov.tm.repository.TaskRepository;
import ru.ekfedorov.tm.repository.UserRepository;
import ru.ekfedorov.tm.service.*;
import ru.ekfedorov.tm.util.TerminalUtil;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;


public class Bootstrap implements ServiceLocator {

    public final ICommandRepository commandRepository = new CommandRepository();

    public final ICommandService commandService = new CommandService(commandRepository);

    public final ILoggerService loggerService = new LoggerService();

    public final IProjectRepository projectRepository = new ProjectRepository();

    public final IProjectService projectService = new ProjectService(projectRepository);

    public final ITaskRepository taskRepository = new TaskRepository();

    public final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    public final ITaskService taskService = new TaskService(taskRepository);

    public final IUserRepository userRepository = new UserRepository();

    public final IUserService userService = new UserService(userRepository);

    public final IAuthService authService = new AuthService(userService);

    {
        registry(new AboutCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearAllCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectRemoveWithTasksByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindByProjectIdCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearAllCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFindAllByProjectIdCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindByProjectIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserIsAuthCommand());
        registry(new UserChangePassCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserClearAllCommand());
        registry(new UserLockByLoginCommand());
        registry(new UserUnlockByLoginCommand());
    }

    private void defaultData() throws Exception {
        final String userId = userRepository.findByLogin("test").get().getId();
        projectService.add(userId, "bbb", "-").setStatus(Status.IN_PROGRESS);
        projectService.add(userId, "bab", "-").setStatus(Status.COMPLETE);
        projectService.add(userId, "aaa", "-").setStatus(Status.NOT_STARTED);
        projectService.add(userId, "ccc", "-").setStatus(Status.COMPLETE);
        taskService.add(userId, "bbb", "-").setStatus(Status.IN_PROGRESS);
        taskService.add(userId, "aaa", "-").setStatus(Status.NOT_STARTED);
        taskService.add(userId, "ccc", "-").setStatus(Status.COMPLETE);
        taskService.add(userId, "eee", "-").setStatus(Status.NOT_STARTED);
        taskService.add(userId, "ddd", "-").setStatus(Status.IN_PROGRESS);
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    private void initUser() throws Exception {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void parseArg(final String arg) throws Exception {
        if (isEmpty(arg)) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    private boolean parseArgs(final String... args) throws Exception {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        parseArg(param);
        return true;
    }

    private void parseCommand(final String cmd) throws Exception {
        if (isEmpty(cmd)) return;
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new IncorrectCommandException(cmd);
        final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String... args) throws Exception {
        loggerService.debug("***           TEST          ***");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        initUser();
        defaultData();
        if (parseArgs(args)) new ExitCommand().execute();
        while (true) {
            try {
                System.out.println();
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

}
