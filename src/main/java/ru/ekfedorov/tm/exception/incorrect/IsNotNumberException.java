package ru.ekfedorov.tm.exception.incorrect;

import ru.ekfedorov.tm.exception.AbstractException;

public final class IsNotNumberException extends AbstractException {

    public IsNotNumberException(String value) throws Exception {
        super("Error! " + value + " is not number...");
    }

}
