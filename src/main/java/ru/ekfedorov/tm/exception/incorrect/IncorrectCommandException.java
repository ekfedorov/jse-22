package ru.ekfedorov.tm.exception.incorrect;

import ru.ekfedorov.tm.exception.AbstractException;

public final class IncorrectCommandException extends AbstractException {

    public IncorrectCommandException(String command) throws Exception {
        super("Error! Command ``" + command + "`` not found...");
    }

}
