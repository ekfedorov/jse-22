# TASK MANAGER

Console application fo task list.

# DEVELOPER INFO

NAME: Evgeniy Fedorov
 
EMAIL: ekfedorov@tsconsulting.com

# SOFTWARE

* JDK 1.8

* MAVEN 3.6.3

* WINDOWS 10

# HARDWARE

* RAM 16Gb

* CPU i5 8th Gen

* SSD 512Gb

# BUILD PROGRAM

```
mvn clean install
```

# RUN PROGRAM

```
java -jar ./task-manager.jar
```

# BUILT WITH
* IntelliJ IDEA 2020.03 Community Edition - The IDE used

# SCREENSHOTS
https://yadi.sk/d/inSz8MalQJIuhA?w=1